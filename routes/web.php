<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

//AuthController
Route::post('login', 'AuthController@login');
Route::get('logout', 'AuthController@logout');

//ScrapesController
Route::get('scrapes/all', 'ScrapesController@all');
Route::get('scrapes/load', 'ScrapesController@load');
Route::resource('scrapes', 'ScrapesController');
