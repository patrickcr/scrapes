<?php

namespace App\Authentication;

use App\FormResult;
use App\Validation\ValidateRequestsForApp;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Authentication
{
    use ValidateRequestsForApp;

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function login()
    {
        return $this->validateLoginRequest()->authenticate();

    }


    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }


    public function messages()
    {
        return [
            'email.required' => 'Email is a required field',
            'email.exists' => 'Cannot find this email in the System',
            'password.required' => 'Password is a required field',
            'password.min' => 'Password Min Length is 6 Charaters',
        ];
    }

    public function validateLoginRequest()
    {

        $this->validate($this->request, [
            'email' => 'required|email|exists:users',
            'password' => 'required|min:6'
        ], $this->messages());


        return $this;

    }


    public function authenticate()
    {
        $form = new FormResult(false);

        if (Auth::attempt(['email' => $this->request->email, 'password' => $this->request->password])) {
            // Authentication passed...
            $form->result = true;
            $form->data = Auth::User();
        }

        return new JsonResponse($form);
    }


}