<?php

namespace App\Http\Controllers;

use App\Authentication\Authentication;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(Authentication $auth)
    {
        return $auth->login();
    }

    public function logout(Authentication $auth){

        return $auth->logout();
    }
}
