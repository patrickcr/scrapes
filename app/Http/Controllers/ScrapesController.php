<?php

namespace App\Http\Controllers;

use App\Scrape;
use App\Scrapes\ScrapesBL;
use Illuminate\Http\Request;

class ScrapesController extends Controller
{

    public function __construct()
    {
        $this->middleware('authenticated');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('scrapes.index');
    }

    /**
     * Display a listing of the itens.
     *
     * @return \App\Dataset
     */
    public function all(Request $request)
    {
        $scrapes = Scrape::Page($request);
        return $scrapes->ToJson();
    }

    /**
     * Display a listing of the itens.
     *
     * @return \App\FormResult
     */
    public function load(Request $request, ScrapesBL $scrapesBL)
    {

        return $scrapesBL->loadContents();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ScrapesBL $scrapesBL)
    {
        return $scrapesBL->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, ScrapesBL $scrapesBL)
    {
        $scrape = $scrapesBL->edit($id);
        return view('scrapes.edit')->with("scrape", $scrape);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, ScrapesBL $scrapesBL)
    {
        return $scrapesBL->delete($id)->toJson();
    }
}
