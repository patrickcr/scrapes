<?php

namespace App\Scrapes;

use App\FormResult;
use App\Scrape;
use App\Validation\ValidateRequestsForApp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class ScrapesBL
{
    use ValidateRequestsForApp;

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function loadContents()
    {
        return $this->validateRequest()->loadContent();
    }

    public function save()
    {
        return $this->validateRequest()->saveDB();
    }


    public function messages()
    {
        return [
            'url.required' => 'The Url is a required field',
            'url.unique' => 'This Url already exists in the system',
        ];
    }

    public function validateRequest()
    {

        $this->validate($this->request, [
            'url' => [
                'required',
                Rule::unique('scrapes')->ignore($this->request->id),
            ],
        ], $this->messages());


        return $this;

    }

    public function loadContent()
    {

        $result = new FormResult();

        $crl = curl_init();
        $timeout = 5;
        curl_setopt($crl, CURLOPT_URL, $this->request->url);
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($crl, CURLOPT_CONNECTTIMEOUT, $timeout);
        $result->data = curl_exec($crl);
        curl_close($crl);


        return $result->toJson();

    }

    public function saveDB()
    {
        $result = new FormResult();

        try {
            if ($this->request->id > 0)
                $scrape = Scrape::find($this->request->id);
            else
                $scrape = new Scrape();

            $scrape->fill($this->request->all());
            $scrape->user_id = Auth::User()->id;
            $scrape->save();
        } catch (\Exception $ex) {
            $result->result = false;
            $result->data = $ex->getMessage();
        }

        return $result->toJson();

    }

    public function edit($id)
    {

        if ($id > 0)
            $scrape = Scrape::find($id);
        else {
            $scrape = new Scrape();
            $scrape->id = -1;
            $scrape->url = '';
            $scrape->content = '';
        }

        return $scrape;
    }

    public function delete($id)
    {
        $result = new FormResult();
        try {

            $scrape = Scrape::find($id);
            $scrape->delete();

        } catch (\Exception $ex) {
            $result->result = false;
            $result->data = $ex->getMessage();
        }

        return $result;
    }
}