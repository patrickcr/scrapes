<?php 
namespace App;

class Dataset {

	public $data;
	public $count;
	public $pages;
	

	function __construct($data = null, $skip= null, $take = null)
	{
		if($data != null)
		{
			$this->count = $data->count();
			$this->pages =  ceil($this->count / intval($take));
			$this->data = $data->skip($skip)->take($take)->get();
		}

		return $this;
	}
	public function toJson()
	{
		return response()
            ->json($this);

	}
}

?>