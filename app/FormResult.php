<?php

namespace App;
	
class FormResult 
{
	public $result = true;
	public $fields = [];
	public $data =[];

	function __construct($result = true, $fields = [])
	{
		$this->result = $result;
		$this->fields = $fields;
	}

	public function addField($field)
	{
		$this->fields[$field->name] = $field;
	}

	public function toArray()
	{
		return get_object_vars($this);
	}

	public function toJson(){

		return response()
            ->json($this);
	}
}

?>