<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scrape extends Model
{
    protected $guarded = ['id'];
    protected $fillable = [];

    public function scopePage($query, $request)
    {

        $skip = $request->get("skip");
        $take = $request->get("take");
        $filters = $request->get("filters");

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {

                if ($value != '') {
                    $query = $query->where($key, 'LIKE', '%' . $value . '%');
                }
            }
        }

        $sorts = $request->get("sorts");
        if (!empty($sorts)) {
            foreach ($sorts as $key => $value) {


                if ($value != '') {

                    $query = $query->OrderBy($key, $value);

                }
            }
        }


        return new Dataset($query, $skip, $take);

    }
}
