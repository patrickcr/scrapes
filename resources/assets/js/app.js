/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('login', require('./views/auth/login.vue'));
Vue.component('noty', require('./components/Noty.vue'));


Vue.component('view-scrapes', require('./views/scrapes/index.vue'));
Vue.component('view-scrapes-edit', require('./views/scrapes/edit.vue'));


const app = new Vue({
    el: '#app',
    data: {
        notifications: []
    },

    methods: {
        setUpNotifications: function () {

            var local = this;
            local.notifications.forEach(function (noty, index) {

                if (noty.timeout != null) {
                    setTimeout(function () {

                        local.notifications.$remove(noty);
                    }, noty.timeout)
                }
            })
        },
    },
    events: {

        noty: function (noty) {
            console.log(noty);
            this.notifications.push(noty);
            this.setUpNotifications();
        },
        execute: function (fn, noty) {

            var fn_name = fn.substring(0, fn.indexOf('('));
            var params = fn.substring(fn.indexOf('(') + 1, fn.indexOf(')'));

            this.$broadcast(fn_name, params);
            if (noty != null)
                this.notifications.$remove(noty);

        }
    }
});
