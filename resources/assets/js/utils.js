module.exports = {
    Field: function () {
        return {
            name: null,
            class: null,
            msg: null,
            state: null
        }
    }
    ,

    Noty: function (_type, _message, _timeout, _fn) {
        return {
            type: _type,
            message: _message,
            timeout: _timeout,
            fn: _fn,
        }
    },

    File: function () {
        return {
            name: '',
            ext: '',
            data: '',

        }
    }
};