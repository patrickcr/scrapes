<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SCRAPE APP</title>
    <link rel="stylesheet" href="../../css/app.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script>
        window.Laravel =
                <?php echo json_encode([
                        'csrfToken' => csrf_token(),
                ]); ?>

                        z = null;
    </script>
</head>

<body>
<div id="app">
    @include('layout.header')
    <div class="container" style="width: 100%">

        <div class="col-xs-12  noty">
            <noty v-for="noty in notifications" :noty="noty"></noty>
        </div>

        @yield("content")
    </div>
</div>
</body>
<script src="../../js/app.js"></script>
</html>