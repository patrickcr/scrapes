@extends('master')
@section('content')

    <view-scrapes-edit :scrape="{{ $scrape->ToJson() }}" inline-template>
        <div class="col-xs-12 hidden" v-bind:class="(loaded) ? '' : 'hidden'">
            <form v-on:submit="save" method="POST" role="form">

                <legend>
                    <span v-if="scrape.id > 0">EDIT SCRAPE</span>
                    <span v-if="scrape.id == -1">NEW SCRAPE</span>
                    <strong>@{{ scrape.url }}</strong></legend>

                <div class="form-group col-md-8 h-90" v-bind:class="fields.url.class">
                    <label for="">Url</label>
                    <input type="text" class="form-control" v-model="scrape.url" placeholder="ex:. www.google.com">
                    <span class="help-block" v-show="!fields.url.state" v-text="fields.url.msg"></span>
                </div>
                <div class="form-group col-md-4">
                    <button type="button" v-on:click="loadContent()" class="btn btn-info" style="position: relative; top: 15px;">LOAD CONTENTS</button>
                    <button type="button" v-on:click="clear()" class="btn btn-info" style="position: relative; top: 15px;">CLEAR</button>
                </div>
                <div class="form-group col-md-12 h-90" v-bind:class="fields.url.class">
                    <iframe id="contents" v-html="scrape.content" style="height: 400px; width: 100%; border: none; overflow: scroll"></iframe>
                </div>

                <div v-if="contentLoaded" class="form-group col-xs-12 tar">
                    <a href="/scrapes" class="btn btn-secondary">CLOSE</a>
                    <button type="submit" class="btn btn-primary">SAVE</button>
                </div>
            </form>
        </div>
    </view-scrapes-edit>
@endsection