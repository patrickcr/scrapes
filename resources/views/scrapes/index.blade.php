@extends('master')
@section('content')


    <div class="view">
        @yield('content')
    </div>


    <view-scrapes inline-template>
        <div class="col-xs-12">
            <div class="list-group list-table">
                {{-- HEADER TITLES --}}
                <div class="list-group-item header">
                    <div class="col-md-1">#</div>
                    <div class="col-md-9">Url</div>
                    <div class="col-md-2 tar">
                        <a href="/scrapes/-1/edit" type="button" class="btn btn-sm btn-primary"
                        ><i class="fa fa-plus" aria-hidden="true"></i> NEW</a>

                    </div>
                </div>

                <div class="list-group-item filters">
                    <div class="col-md-1"></div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" v-model="params.filters.url" v-on:keyup="rebind()"
                               placeholder="type a name"></input>
                    </div>
                    <div class="col-md-2"></div>

                </div>

                {{-- DATA --}}

                <div v-for="item in data" class="list-group-item item">
                    <div class="col-md-1 dinamic"><span class="m-title">ID: </span>@{{ item.id }}</div>
                    <div class="col-md-9 dinamic"><span class="m-title">URL: </span>@{{ item.url }}</div>
                    <div class="col-md-2 tar item-controls">
                        <a v-bind:href="'/scrapes/' + item.id + '/edit'" type="button" class="btn btn-sm btn-primary">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                        <a v-on:click="removeConfirm(item)" type="button" class="btn btn-sm btn-danger">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>

                {{--FOOTER--}}
                <div class="list-group-item list-group-item-primary footer">
                    <div class="col-xs-12 col-md-4">

                        <div class="btn-group">
                            <a v-for="size in page.sizes" v-bind:class="(size== params.take) ? 'active' : ''"
                               v-on:click="
                        changeSize(size)" href="#" class="btn btn-default btn-sm">@{{ size }}</a>
                        </div>


                    </div>
                    <div class="col-xs-12 col-md-offset-4 col-md-4 tar">
                        <div v-if="page.pages.length > 1" class="btn-group">
                            <a v-bind:class="(page == params.page) ? 'active' : ''" v-for="page in page.pages"
                               v-on:click="
                        changePage(page)" href="#" class="btn btn-sm btn-default">@{{ (page + 1) }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </view-scrapes>

@endsection