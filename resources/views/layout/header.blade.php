<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><i class="fa fa-download" aria-hidden="true"></i> SCRAPE</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">

            <ul class="nav navbar-nav navbar-right auth-control">
                @if(Auth::check())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Welcome,
                            {{ Auth::User()->name }}
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/logout"><i style="position: relative;top: 5px;" class="fa fa-power-off fl" aria-hidden="true"></i> &nbsp; Logout</a></li>
                        </ul>
                    </li>
                @endif

            </ul>
        </div>
    </div>
</nav>