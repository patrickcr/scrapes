@extends('master')
@section('content')


    <div class="col-xs-12">
        <div class="tac">
            <h1>SCRAPE APP</h1>
        </div>
        </br>
        <login inline-template>
            <div class="col-xs-12 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4">

                <div class="panel panel-primary auth-panel">
                    <div class="panel-heading" v-on:click="open = !open">
                        <h3 class="panel-title">
                            <i class="fa fa-lock"></i>
                            &nbsp;CLICK TO LOGIN

                        </h3>
                    </div>
                    <div class="panel-body" v-bind:class="(open) ? 'visible' : ''">
                        <div v-show="result" class="alert alert-dismissible alert-success">

                            <strong>Successfull Authenticated !, you wil be redirected...</strong>.
                        </div>
                        <form v-on:submit="Submit" method="post" role="form">
                            <legend>Authenticate</legend>

                            <div class="form-group" v-bind:class="fields.email.class">
                                <label for="">Email</label>
                                <input type="email" v-model="model.email" class="form-control" name="" id="" placeholder="Type your email...">
                                <span class="help-block" v-show="!fields.email.state"
                                      v-text="fields.email.msg"></span>
                            </div>

                            <div class="form-group" v-bind:class="fields.password.class">
                                <label for="">Password</label>
                                <input type="password" v-model="model.password"  class="form-control" name="" id=""
                                       placeholder="Type your password...">
                                <span class="help-block" v-show="!fields.password.state"
                                      v-text="fields.password.msg"></span>
                            </div>

                            <div class="form-group tac">
                                <button type="submit" class="btn btn-lg btn-primary btn-animated-lg w50p">
                                    <i id="btn_loader" v-bind:class="(loading) ? '' : 'hidden'"
                                       class="fa fa-gear faa-spin animated hidden"></i>
                                    LOGIN</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </login>
    </div>

@endsection
